/**
  ******************************************************************************
  * @file           : ads1298.h
  * @brief          : Define any functions & variables related to the ADS chip
  ******************************************************************************
  * @attention
  *	- 	I try to keep all the ADS-dependent variable as the static members 
	*			to have a better architecture and consequently, more reusability features.
	*
  ******************************************************************************
  */
#ifndef __ADS1298_H__
#define __ADS1298_H__

void initialADS1298(void);
 
void ADSSendSignal(void);
	
#endif

