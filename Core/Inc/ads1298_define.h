/**
  ******************************************************************************
  * @file           : ads1298_defines.h
  * @brief          : ADS1298 defines are declared here.
  ******************************************************************************
  * @attention
  *	-		All defines related to the ADS module should be declared here.
	*
  ******************************************************************************
  */
	
#ifndef	__ADS1298_DEFINES_H__	
#define	__ADS1298_DEFINES_H__

/* Timeout (ms) */
#define		ADS_RESET_PER			1000
#define		SPI_TOUT					1000

/* Sampling decreament rate */
#define		DOWN_SAMPL_RATE		125								/* ADS is setup on 250 sam/sec, so 0.5s = 125 samples */

/* Offset of writing 4 sample bytes within the ADS sample */
#define		SAM_OFF						10

/* ADS sample length (Byte)*/
#define		ADS_SAMP_LEN			27


#endif
