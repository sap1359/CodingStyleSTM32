/**
  ******************************************************************************
  * @file           : flash.h
  * @brief          : Define flash-related functions 
  ******************************************************************************
  * @attention
  *	-		The required function to interacting with the flash memory are declared here. 
	*
  ******************************************************************************
  */
	
	#include "stdint.h"
	#include "datatype.h"
	#include "main.h"
	
	#ifndef __FLASH_H__
	#define __FLASH_H__
	
	void initialFlash(void);
	
	HAL_StatusTypeDef writeSample(uint8_t* data); 
	uint32_t retriveUnreadData(uint8_t* data);
	
	void eraseStorageAre(void);
		
	#endif
