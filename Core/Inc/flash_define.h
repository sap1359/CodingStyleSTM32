/**
  ******************************************************************************
  * @file           : flash_define.h
  * @brief          : Flash-related defines are declared here.
  ******************************************************************************
  * @attention
  *	-		All defines related to the Flash module should be declared here.
	*
  ******************************************************************************
  */

/**	As it is illustrated here, in order to keep the integrity to write
	* on the flash memory (for example to handle the incomplete writin in the case of the 
	* power-outage, I propose to use a data structure to writing in the flash, which is 
	*	started by 0X55 (Start Byte) and ends-up with the 0XAA (End Byte) to ensure the 
	*	complete writing on the internal memory.
	* There is also a Read Status byte which programmed to 88, when the data is read and transfered 
	*	to the PC using UART. It should be nited that the collected data mark as read and are not 
	*	send in the next round of collecting data.
	* The permanent data stored in the flash can be erased by UART command.
	*/
	
/**	1 Byte			- 1Byte	- 4 Bytes	-	1Byte	  >>   Totally 7 Bytes to store 4 Bytes of data
	* READ STATUS	-	START -	DATA 		-	END 	
	* FF: READED
	*	88: UNREADED
	* FF OR 0X88	|	0X55	|	XXYYZZWW			|	AA	
	*/		

#ifndef __FLASH_DEFINE_H__
#define __FLASH_DEFINE_H__

/* Internal flash (Memory address)*/
#define		FLASH_STR_ADDR		0x080E0000		/* Start of the Sector 11 */
#define		FLASH_END_ADDR		0x080EFFFF		/* End of the saving area */
#define		FLASH_ARE_SECTOR	FLASH_SECTOR_11

/* Maximum values */
#define		MAX_RETRY					5							/* To write a byte in the Flash memory */

/* Lengths (Byte)*/
#define		FLAS_FRM_TOT_LEN	7
#define		FLAS_FRM_DAT_LEN	4

/* Offsets in a data sample */
#define		FLSH_STUS_OFS			0
#define		FLSH_STRT_OFS			1
#define		FLSH_DATA_OFS			2
#define		FLSH_END_OFS			6

/* Pre-defined values */
#define		UNPROGRAMED_DATA	0xFF
#define		FLSH_DATA_READ		0x88
#define		FLSH_DATA_STATRT	0x55
#define		FLSH_DATA_END			0xAA

#endif
