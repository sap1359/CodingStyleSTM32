/**
  ******************************************************************************
  * @file           : packet.h
  * @brief          : Define any packet-dependent functions & variables
  ******************************************************************************
  * @attention
	*	-		The packet module is a helper library.
	*
  ******************************************************************************
  */
	
#include "stdint.h"
	
#ifndef __PACKET_H__
#define __PACKET_H__

enum uartCommand getCommand(uint8_t* packet_data, int len);

int createACKPackage(uint8_t* packet_data);
int createNACKPackage(uint8_t* packet_data);
int createSAMPPackage(uint8_t* packet_data);
int createFULLPackage(uint8_t* packet_data);
int createNODATAPackage(uint8_t* packet_data);

#endif
