/**
  ******************************************************************************
  * @file           : packet_defines.h
  * @brief          : Packet-related defines are declared here.
  ******************************************************************************
  * @attention
  *	-		All defines related to the Packet module should be declared here.
	*
  ******************************************************************************
  */
	
#ifndef __PACKET_DEFINE_H__
#define __PACKET_DEFINE_H__

/* Packet sizes (Byte)*/
#define		ACK_PACK_LEN			2								/* OK */	
#define		NACK_PACK_LEN			4								/* NACK */
#define		SAMP_PACK_LEN			4								/* SAMP */
#define		FULL_PACK_LEN			4								/* FULL */
#define		NODATA_PACK_LEN		6								/* NODATA */

/*
*	I prefer to use define for text commands, however, in this code I use 
*	the string UART command as you clearly declared in the requirement document.
*	The better implementation can be proposed using binary send/receive, 
*	which can be implemented using enum, perfectely.
*/

/* Received Packets */									
#define		PAC_SEND_SAMP			"SEND_SAMP"			
#define 	PAC_START_SAMP		"START_SAMP"
#define 	PAC_STOP_SAMP			"STOP_SAMP"
#define		PAC_ERASE					"ERASE"

/* Sent Packets */
#define		PAC_ACK						"OK"
#define		PAC_NACK					"NACK"
#define		PAC_FULL					"FULL"
#define		PAC_SAMP					"SAMP"
#define		PAC_NODATA				"NODATA"

#endif
