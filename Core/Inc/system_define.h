/**
  ******************************************************************************
  * @file           : system_define.h
  * @brief          : System-related defines are declared here.
  ******************************************************************************
  * @attention
  *	-		All defines related to the whole system attributes should be declared here.
  ******************************************************************************
  */
	
#ifndef __SYSTEM_DEFINE_H__
#define __SYSTEM_DEFINE_H__

/* Task's stack sizes (Byte) */
#define	DEF_TASK_STK_SIZ		512
#define	UARTRX_TASK_STK_SIZ	2048
#define	UARTTX_TASK_STK_SIZ	2048
#define	BUTN_TASK_STAK_SIZ	512
#define SAMP_TASK_STAK_SIZ	2048

/* Signal flags (bit position)*/
#define		BTN_SIG_FLAG			0x0001
#define		SAM_SIG_FLAG			0x0002

/* Intervals (mS)*/
#define		LED_IDLE_ITRVL		1000	
#define		LED_SAMP_ITRVL		500
#define		LED_FFUL_ITRVL		250
#define		DEBOUNCE_TIME			50
#define 	SHORT_PER					1

#endif
