/**
  ******************************************************************************
  * @file           : uart.h
  * @brief          : Define any UART-dependent functions & variables
  ******************************************************************************
  * @attention
  *	- 	I try to keep the uart-dependent variable as static members 
	*			to have a better architecture and consequently, more reusability features.
	*
  ******************************************************************************
  */
	
#include "stdint.h"	
	
#ifndef __UART_H__
#define __UART_H__

void initialUART(void);

void uartSend(uint8_t* data, int len);

void uartSendFullMessage(void);
void uartSendSamplingMessage(void);

#endif
