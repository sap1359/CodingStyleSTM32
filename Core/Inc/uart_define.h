/**
  ******************************************************************************
  * @file           : uart_defines.h
  * @brief          : UART-related defines are declared here.
  ******************************************************************************
  * @attention
  *	-		All defines related to the UART module should be declared here.
	*
  ******************************************************************************
  */
	
#ifndef __UART_DEFINE_H__
#define __UART_DEFINE_H__

/* Timeout & Intervals (ms) */
#define		UART_RX_INTRVL		1	
#define		UART_TX_INTRVL		500	
#define		UART_TOUT					2000

/* Lengths (Byte)*/
#define		UART_RX_LEN				100
#define		UART_TX_LEN				1000
#define		UART_DMA_LEN			1	
#define		UART_MAX_DMA_LEN 	20							/* It is defined to more flexibility and reusability of uart module (i.e. uart.c & uart.h) */

#endif
