/**
  ******************************************************************************
  * @file           : ads1298.c
  * @brief          : All the required functions related to ADS1298 are declares here.
  ******************************************************************************
  * @important		
	*	1.	This file contains a critical section of two writers at the same time.
	*			The source of race condition is the extern variable >>mode<<, which is 
	*			handled with the >>mutex_mode_id<< Mutex.
	*			The other parts of the critical section are implemented in main.c, 
	*			uart.c, and ads1298.c.
	*
	*	2.	In this module an OS-Signal is used to connect to the ISR.
	*					
	******************************************************************************
  */

#include "main.h"
#include "String.h"
#include "Stdint.h"
#include "cmsis_os.h"
#include "flash.h"
#include "ads1298_define.h"
#include "system_define.h"

extern SPI_HandleTypeDef hspi2;
/*-----*/

void samplingTask(void const * argument);
osThreadId samplingTaskHandle;
/*-----*/


/**
	* @brief  		This function reset the ADS1298 chip.
	* @param  		None.
  * @retval 		None.
  */
static void ADSReset(void)
{
	HAL_GPIO_WritePin(ADS_RST_GPIO_Port, ADS_RST_Pin, GPIO_PIN_SET); 		/* Set the Reset pin high */
	
	osDelay(ADS_RESET_PER);																							/* Wait (according to the datasheet) */
	
	HAL_GPIO_WritePin(ADS_RST_GPIO_Port, ADS_RST_Pin, GPIO_PIN_RESET); 	/* Set the Reset pin low */
	
	osDelay(ADS_RESET_PER);																							/* Wait (according to the datasheet) */

	HAL_GPIO_WritePin(ADS_RST_GPIO_Port, ADS_RST_Pin, GPIO_PIN_SET); 		/* Set the Reset pin high - Here, the ADS chip is activated */
	
	osDelay(SHORT_PER);																									/* Wait (according to the datasheet) */
}
  

/**
	* @brief  		This function activate the sampling on ADS1298 chip, using hardware signal.
	*	@attention	There are also the other method to start sampling using SPI command.
	* @param  		None.
  * @retval 		None.
  */
static void hardStartSampling(void)
{
	HAL_GPIO_WritePin(ADS_START_GPIO_Port, ADS_START_Pin,  GPIO_PIN_SET); 	
}	


/**
	* @brief  		This function creates the sampling task & ID.
	* @param  		None.
  * @retval 		None.
  */
void initialADS1298(void)
{
	osThreadDef(samplingTask, samplingTask, osPriorityHigh, 1, SAMP_TASK_STAK_SIZ);
	samplingTaskHandle = osThreadCreate(osThread(samplingTask), NULL);
}


/**
	* @brief  		This function send signal to the running task (samplingTask).
	* @attention	This function is used to keep modularity. Using this function
	*							the ocal variable of >>samplingTaskHandle<< keep remains local (i.e. static).
	* @param  		None.
  * @retval 		None.
  */
void ADSSendSignal(void)
{
	osSignalSet(samplingTaskHandle, SAM_SIG_FLAG);				/* send a Signal to debounceTask function to activate the barrier */
}


/**
	* @brief  		Function implementing the sampling mechanism.
	* @attention	This function communicate with the HAL_GPIO_EXTI_Callback
	*							using signal/wait mechanism.
	*							This task is not control using dely, it is controlled by signal.
	*	@important	This function is a part of critical section due to using 
	*							global variable >>mode<<. Another part is located in uart.c & main.c
	* @param  		argument: Not used.
  * @retval 		None.
  */
void samplingTask(void const * argument)
{
	int ctr;
	osEvent event;
	enum deviceMode dev_mode;
	HAL_StatusTypeDef er_code;
	static uint8_t sampleBuffer[ADS_SAMP_LEN] ;
	
	osDelay(SHORT_PER);																		/* Time to warm-up the ADS chip */
	
	/* Initializing the ADS chip */
	ADSReset();																						/* Reset the ADS chip */
	hardStartSampling();																	/* Start sampling */
	
	osDelay(SHORT_PER);
	
	ctr = 0;
	
  for(;;)		/* task loop */
  {
		event = osSignalWait(SAM_SIG_FLAG, osWaitForever);										/* Wait for signal from HAL_GPIO_EXTI_Callback */
		
		if (event.status == osEventSignal)																		/* Validating signal */
		{
			dev_mode = getMode();																								/* Acuire the device mode */													
			
			ctr++;
			if (ctr > DOWN_SAMPL_RATE)																					/* To adjust sampling every 0.5 second. ADS sampling rate is too higher ;) */
			{
				if (dev_mode == sampling)																					/* If device is in the sampling mode, do sampling stuffs */
				{
					HAL_SPI_Receive(&hspi2, sampleBuffer, ADS_SAMP_LEN, SPI_TOUT);
					er_code = writeSample(sampleBuffer + SAM_OFF);
				
					if (er_code == HAL_ERROR)																				/* If the Flash area is full, change the device mode to Full */
						setMode(flashFull);																						/* >>> Critical section <<< - mode is the source of race condition.  */												
				}
				
				ctr = 0;																					/* Adjust down sampling for the next time */
			}
		}
  }
}
