/**
  ******************************************************************************
  * @file           : gpio_interrupt.c
  * @brief          : Handling the shared GPIO interrupt routine
  ******************************************************************************
  * @important		
	*	1.	Due to the fact that both ADS & Push-Button GPIO interrupts handle in the
	*			same ISR, to preserved modularity the ISR routine is located in an independent
	*			source file.
	*					
	******************************************************************************
  */
	
#include "main.h"
#include "button.h"
#include "ads1298.h"


/**
  * @brief  		This is the overrided function of external interrupt
	* @attention	Don't call time-consuming jobs in this function. 
	*							It is executed in the interrupt context.
  * @param  		argument: The GPIO pin which causes interrupt.
  * @retval 		None.
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{ 		
	if (GPIO_Pin == BUTTON_Pin)
	{
		buttonSendSignal();
	}
	else if (GPIO_Pin == ADS_DRDY_Pin)
	{
		ADSSendSignal();
	}
}
